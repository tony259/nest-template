import { CoreModule, WebinarModule } from '@nest-template/core';
import { Module } from '@nestjs/common';
import { UserController } from './user.controller';

@Module({
  imports: [CoreModule, WebinarModule],
  controllers: [UserController],
})
export class AppModule {}
