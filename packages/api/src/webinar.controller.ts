import { WebinarHandler } from '@nest-template/core';
import { Body, Controller, Param, Post } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { CreateWebinarDto } from './dtos/create-webinar.dto';
import { WebinarDto } from './dtos/webinar.dto';

@Controller('organizers/events/:eventId/webinars')
export class WebinarController {
  constructor(private readonly webinarHandler: WebinarHandler) {}

  @Post()
  async create(
    @Param('eventId') eventId: string,
    @Body() dto: CreateWebinarDto,
  ) {
    const webinar = await this.webinarHandler.create({
      ...dto,
      eventId,
    });

    return plainToClass(WebinarDto, webinar);
  }
}
