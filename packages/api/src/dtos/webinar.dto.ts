import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class WebinarDto {
  @Expose()
  id: string;

  @Expose()
  name: string;
}
