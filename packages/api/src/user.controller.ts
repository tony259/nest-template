import { UserHandler, UserQuerier } from '@nest-template/core';
import { Body, Controller, Get, Post } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { UserSignUpDto } from './dtos/user-sign-up.dto';
import { UserDto } from './dtos/user.dto';

@Controller('users')
export class UserController {
  constructor(
    private readonly userQuerier: UserQuerier,
    private readonly userHandler: UserHandler,
  ) {}

  @Get()
  async findAll() {
    const users = await this.userQuerier.findAll();
    return users.map((user) => plainToClass(UserDto, user));
  }

  @Post()
  async signUp(@Body() dto: UserSignUpDto) {
    const user = await this.userHandler.signUp(dto);
    return plainToClass(UserDto, user);
  }

  @Post(':id/change-password')
  async changePassword(@Body() dto: UserSignUpDto) {
    const user = await this.userHandler.changePassword(dto);
    return plainToClass(UserDto, user);
  }
}
