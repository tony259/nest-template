import { OrganizationHandler } from '@nest-template/core';
import { Body, Controller, Post } from '@nestjs/common';
import { User } from '@prisma/client';
import { plainToClass } from 'class-transformer';
import { CurrentUser } from './decorators/current-user.decorator';
import { CreateOrganizationDto } from './dtos/create-organization.dto';
import { OrganizationDto } from './dtos/organization.dto';

@Controller('organizations')
export class OrganizationController {
  constructor(private readonly organizationHandler: OrganizationHandler) {}

  @Post()
  async create(
    @CurrentUser() currentUser: User,
    @Body() dto: CreateOrganizationDto,
  ) {
    const organization = await this.organizationHandler.create({
      ...dto,
      createdByUserId: currentUser.id,
    });

    return plainToClass(OrganizationDto, organization);
  }
}
