export * from './adapter/prisma/prisma.module';
export * from './domain/core/core.module';
export * from './domain/webinar/webinar.module';
