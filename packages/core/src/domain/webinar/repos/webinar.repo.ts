import { Injectable } from '@nestjs/common';
import { Webinar } from '@prisma/client';
import { PrismaService } from 'src/adapter/prisma/prisma.service';

@Injectable()
export class WebinarRepository {
  constructor(private readonly prisma: PrismaService) {}

  async create(data: Pick<Webinar, 'name' | 'eventId'>): Promise<Webinar> {
    return this.prisma.webinar.create({ data });
  }
}
