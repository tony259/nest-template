import { Injectable } from '@nestjs/common';
import { Webinar } from '@prisma/client';
import { WebinarRepository } from '../repos/webinar.repo';

@Injectable()
export class WebinarHandler {
  constructor(private readonly webinarRepo: WebinarRepository) {}

  async create(data: Pick<Webinar, 'name' | 'eventId'>): Promise<Webinar> {
    return this.webinarRepo.create(data);
  }
}
