import { Module } from '@nestjs/common';
import { PrismaModule } from 'src/adapter/prisma/prisma.module';
import { WebinarHandler } from './handlers/webinar.handler';
import { WebinarRepository } from './repos/webinar.repo';

@Module({
  imports: [PrismaModule],
  providers: [WebinarRepository, WebinarHandler],
  exports: [WebinarHandler],
})
export class WebinarModule {}

export { WebinarHandler };
