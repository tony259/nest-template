import { Injectable } from '@nestjs/common';
import { User } from '@prisma/client';
import { PrismaService } from 'src/adapter/prisma/prisma.service';

@Injectable()
export class UserRepository {
  constructor(private readonly prisma: PrismaService) {}

  async create(user: Pick<User, 'email' | 'password'>): Promise<User> {
    return this.prisma.user.create({ data: user });
  }

  async update(user: User): Promise<User> {
    return this.prisma.user.update({ where: { id: user.id }, data: user });
  }

  async findByEmail(email: string): Promise<User | undefined> {
    return this.prisma.user.findFirst({ where: { email } });
  }
}
