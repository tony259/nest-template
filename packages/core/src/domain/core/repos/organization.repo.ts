import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/adapter/prisma/prisma.service';
import { Organization } from '@prisma/client';

@Injectable()
export class OrganizationRepository {
  constructor(private readonly prisma: PrismaService) {}

  async create(
    organization: Pick<Organization, 'name' | 'createdByUserId'>,
  ): Promise<Organization> {
    return this.prisma.organization.create({ data: organization });
  }

  async update(organization: Organization): Promise<Organization> {
    return this.prisma.organization.update({
      where: { id: organization.id },
      data: organization,
    });
  }
}
