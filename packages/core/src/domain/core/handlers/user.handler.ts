import { Injectable } from '@nestjs/common';
import { User } from '@prisma/client';
import { UserRepository } from '../repos/user.repo';

@Injectable()
export class UserHandler {
  constructor(private readonly userRepo: UserRepository) {}

  async signUp(user: Pick<User, 'email' | 'password'>): Promise<User> {
    const existingUser = await this.userRepo.findByEmail(user.email);

    if (existingUser) {
      throw new Error('User already exists');
    }

    return this.userRepo.create(user);
  }

  async changePassword(data: Pick<User, 'email' | 'password'>): Promise<User> {
    const user = await this.userRepo.findByEmail(data.email);

    if (!user) {
      throw new Error('User not found');
    }

    user.password = data.password;

    return this.userRepo.update(user);
  }
}
