import { Injectable } from '@nestjs/common';
import { Organization } from '@prisma/client';
import { OrganizationRepository } from '../repos/organization.repo';

@Injectable()
export class OrganizationHandler {
  constructor(private readonly organizationRepo: OrganizationRepository) {}

  async create(
    organization: Pick<Organization, 'name' | 'createdByUserId'>,
  ): Promise<Organization> {
    return this.organizationRepo.create(organization);
  }

  async update(organization: Organization): Promise<Organization> {
    return this.organizationRepo.update(organization);
  }
}
