import { Module } from '@nestjs/common';
import { PrismaModule } from 'src/adapter/prisma/prisma.module';
import { OrganizationHandler } from './handlers/organization.handler';
import { UserHandler } from './handlers/user.handler';
import { UserQuerier } from './queriers/user.querier';
import { OrganizationRepository } from './repos/organization.repo';
import { UserRepository } from './repos/user.repo';

@Module({
  imports: [PrismaModule],
  providers: [
    UserRepository,
    UserQuerier,
    UserHandler,
    OrganizationRepository,
    OrganizationHandler,
  ],
  exports: [UserQuerier, UserHandler, OrganizationHandler],
})
export class CoreModule {}

export { UserQuerier, UserHandler, OrganizationHandler };
