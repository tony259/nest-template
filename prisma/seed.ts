import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

async function main() {
  const user = await prisma.user.create({
    data: {
      email: 'test@test.com',
      password: 'test',
    },
  });

  const organization = await prisma.organization.create({
    data: {
      name: 'organization A',
      createdByUserId: user.id,
    },
  });

  const event = await prisma.event.create({
    data: {
      name: 'event A',
      organizationId: organization.id,
      createdByUserId: user.id,
    },
  });

  const webinar = await prisma.webinar.create({
    data: {
      name: 'webinar A',
      eventId: event.id,
    },
  });

  console.log({
    user,
    organization,
    event,
    webinar,
  });
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    // close Prisma Client at the end
    await prisma.$disconnect();
  });
